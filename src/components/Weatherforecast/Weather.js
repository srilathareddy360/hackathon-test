import React, { useEffect, useState } from 'react';
import "./Weather.css";
import axios from "axios";



const Weather = () => {
  const [celscius, setCelscius] = useState(false);
  const [data, setAllData] = useState({})
  const handleUnit = (unit)=>{
    console.log(unit)
    const target = document.getElementsByClassName("unit")
    
    switch (unit) {
      case "c":
        setCelscius(true)
        target[0].classList.remove("active")
        target[1].classList.add("active")
        break;
      case "f":
        setCelscius(false)
        target[1].classList.remove("active")
        target[0].classList.add("active")
        break;
      default:setCelscius(false)
        break;
    }
  }
  //fetching the data using axios
  useEffect(() => {
    axios.get(URL
    )
    .then(res=>{
      setAllData(res.data)
      console.log(res)
    })
    .catch(err=>{
      console.log(err)
    })
  }, [])
  const displaySevenDayWeatherCards = 
  Object.keys(data).length >0 ?
      data.forecast.forecastday.map((item,i)=>{
        return <div key={i}>
          <img src={item.day.condition.icon} alt="cloud" />
          <p>{celscius?item.day.avgtemp_c:item.day.avgtemp_f} {celscius?<><sup>o</sup>C</>:<><sup>o</sup>F</>}</p>
          <p>{item.day.condition.text}</p>
        </div>
      })
      :""

  return (
    
    <div className="main-container">
      <div className='container'>
      <h1 className='heading'>Hyderabad Weather Forecast <span className="spantext">Andhra Pradesh,India.</span></h1>
      <div className='temperature_units'>
            <p className='unit active' onClick={()=>handleUnit("f")}><sup>o</sup>F</p>
            <p className='unit' onClick={()=>handleUnit("c")}><sup>o</sup>C</p>
          </div>
      
      </div>
      
     
    <div className="Weather-forecast">
      <div className="container-forecast">
    <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" fill="currentColor"  class="bi bi-cloud-drizzle icon" viewBox="0 0 16 16">
  <path d="M4.158 12.025a.5.5 0 0 1 .316.633l-.5 1.5a.5.5 0 0 1-.948-.316l.5-1.5a.5.5 0 0 1 .632-.317zm6 0a.5.5 0 0 1 .316.633l-.5 1.5a.5.5 0 0 1-.948-.316l.5-1.5a.5.5 0 0 1 .632-.317zm-3.5 1.5a.5.5 0 0 1 .316.633l-.5 1.5a.5.5 0 0 1-.948-.316l.5-1.5a.5.5 0 0 1 .632-.317zm6 0a.5.5 0 0 1 .316.633l-.5 1.5a.5.5 0 1 1-.948-.316l.5-1.5a.5.5 0 0 1 .632-.317zm.747-8.498a5.001 5.001 0 0 0-9.499-1.004A3.5 3.5 0 1 0 3.5 11H13a3 3 0 0 0 .405-5.973zM8.5 2a4 4 0 0 1 3.976 3.555.5.5 0 0 0 .5.445H13a2 2 0 0 1 0 4H3.5a2.5 2.5 0 1 1 .605-4.926.5.5 0 0 0 .596-.329A4.002 4.002 0 0 1 8.5 2z"/>
</svg>
      <h1 className="text-color">25.8 c Sunday</h1>
      <div className="meters">
      <p className="text-color-para ms-3">wind</p>
      <p className='text-color-para '>18.0 kmph</p>
      </div> 
      <div className="meters">
      <p className="text-color-para ms-3">precip</p>
      <p className='text-color-para '>0.00 kmph</p>
      </div> 
      <div className="meters">
      <p className="text-color-para ms-3">Pressure        </p>
      <p className='text-color-para '>1007.0</p>
      </div> 
</div>
<div class="days-weather">
  <div className="days-count">
<svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" fill="currentColor"  class="bi bi-cloud-drizzle icon" viewBox="0 0 16 16">
  <path d="M4.158 12.025a.5.5 0 0 1 .316.633l-.5 1.5a.5.5 0 0 1-.948-.316l.5-1.5a.5.5 0 0 1 .632-.317zm6 0a.5.5 0 0 1 .316.633l-.5 1.5a.5.5 0 0 1-.948-.316l.5-1.5a.5.5 0 0 1 .632-.317zm-3.5 1.5a.5.5 0 0 1 .316.633l-.5 1.5a.5.5 0 0 1-.948-.316l.5-1.5a.5.5 0 0 1 .632-.317zm6 0a.5.5 0 0 1 .316.633l-.5 1.5a.5.5 0 1 1-.948-.316l.5-1.5a.5.5 0 0 1 .632-.317zm.747-8.498a5.001 5.001 0 0 0-9.499-1.004A3.5 3.5 0 1 0 3.5 11H13a3 3 0 0 0 .405-5.973zM8.5 2a4 4 0 0 1 3.976 3.555.5.5 0 0 0 .5.445H13a2 2 0 0 1 0 4H3.5a2.5 2.5 0 1 1 .605-4.926.5.5 0 0 0 .596-.329A4.002 4.002 0 0 1 8.5 2z"/>
</svg>
<p className="day-text">Monday</p>
<p className='day-text'>28.8 c</p>
<p className='day-text'>Partly Coludy</p>
</div>
<div className="days-count">
  <div className="margin">
<svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" fill="currentColor" style={{marginRight:"50px"}} class="bi bi-cloud-drizzle icon" viewBox="0 0 16 16">
  <path d="M4.158 12.025a.5.5 0 0 1 .316.633l-.5 1.5a.5.5 0 0 1-.948-.316l.5-1.5a.5.5 0 0 1 .632-.317zm6 0a.5.5 0 0 1 .316.633l-.5 1.5a.5.5 0 0 1-.948-.316l.5-1.5a.5.5 0 0 1 .632-.317zm-3.5 1.5a.5.5 0 0 1 .316.633l-.5 1.5a.5.5 0 0 1-.948-.316l.5-1.5a.5.5 0 0 1 .632-.317zm6 0a.5.5 0 0 1 .316.633l-.5 1.5a.5.5 0 1 1-.948-.316l.5-1.5a.5.5 0 0 1 .632-.317zm.747-8.498a5.001 5.001 0 0 0-9.499-1.004A3.5 3.5 0 1 0 3.5 11H13a3 3 0 0 0 .405-5.973zM8.5 2a4 4 0 0 1 3.976 3.555.5.5 0 0 0 .5.445H13a2 2 0 0 1 0 4H3.5a2.5 2.5 0 1 1 .605-4.926.5.5 0 0 0 .596-.329A4.002 4.002 0 0 1 8.5 2z"/>
</svg>
<p className="day-text" style={{margRight:"50px"}}>Tuesday</p>
<p className='day-text'>28.8 c</p>
<p className='day-text'>Partly Coludy</p>
</div>
</div>
<div className="days-count">
<div className="margin">
<svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" fill="currentColor"  class="bi bi-cloud-drizzle icon" viewBox="0 0 16 16">
  <path d="M4.158 12.025a.5.5 0 0 1 .316.633l-.5 1.5a.5.5 0 0 1-.948-.316l.5-1.5a.5.5 0 0 1 .632-.317zm6 0a.5.5 0 0 1 .316.633l-.5 1.5a.5.5 0 0 1-.948-.316l.5-1.5a.5.5 0 0 1 .632-.317zm-3.5 1.5a.5.5 0 0 1 .316.633l-.5 1.5a.5.5 0 0 1-.948-.316l.5-1.5a.5.5 0 0 1 .632-.317zm6 0a.5.5 0 0 1 .316.633l-.5 1.5a.5.5 0 1 1-.948-.316l.5-1.5a.5.5 0 0 1 .632-.317zm.747-8.498a5.001 5.001 0 0 0-9.499-1.004A3.5 3.5 0 1 0 3.5 11H13a3 3 0 0 0 .405-5.973zM8.5 2a4 4 0 0 1 3.976 3.555.5.5 0 0 0 .5.445H13a2 2 0 0 1 0 4H3.5a2.5 2.5 0 1 1 .605-4.926.5.5 0 0 0 .596-.329A4.002 4.002 0 0 1 8.5 2z"/>
</svg>
<p className="day-text">Wednesday</p>
<p className='day-text'>28.8 c</p>
<p className='day-text'>Partly Coludy</p>
</div>
</div>
<div className="days-count">
<div className="margin">
<svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" fill="currentColor"  class="bi bi-cloud-drizzle icon" viewBox="0 0 16 16">
  <path d="M4.158 12.025a.5.5 0 0 1 .316.633l-.5 1.5a.5.5 0 0 1-.948-.316l.5-1.5a.5.5 0 0 1 .632-.317zm6 0a.5.5 0 0 1 .316.633l-.5 1.5a.5.5 0 0 1-.948-.316l.5-1.5a.5.5 0 0 1 .632-.317zm-3.5 1.5a.5.5 0 0 1 .316.633l-.5 1.5a.5.5 0 0 1-.948-.316l.5-1.5a.5.5 0 0 1 .632-.317zm6 0a.5.5 0 0 1 .316.633l-.5 1.5a.5.5 0 1 1-.948-.316l.5-1.5a.5.5 0 0 1 .632-.317zm.747-8.498a5.001 5.001 0 0 0-9.499-1.004A3.5 3.5 0 1 0 3.5 11H13a3 3 0 0 0 .405-5.973zM8.5 2a4 4 0 0 1 3.976 3.555.5.5 0 0 0 .5.445H13a2 2 0 0 1 0 4H3.5a2.5 2.5 0 1 1 .605-4.926.5.5 0 0 0 .596-.329A4.002 4.002 0 0 1 8.5 2z"/>
</svg>
<p className="day-text">Thursday</p>
<p className='day-text'>28.8 c</p>
<p className='day-text'>Partly Coludy</p>
</div>
</div>
<div className="days-count">
<div className="margin">
<svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" fill="currentColor"  class="bi bi-cloud-drizzle icon" viewBox="0 0 16 16">
  <path d="M4.158 12.025a.5.5 0 0 1 .316.633l-.5 1.5a.5.5 0 0 1-.948-.316l.5-1.5a.5.5 0 0 1 .632-.317zm6 0a.5.5 0 0 1 .316.633l-.5 1.5a.5.5 0 0 1-.948-.316l.5-1.5a.5.5 0 0 1 .632-.317zm-3.5 1.5a.5.5 0 0 1 .316.633l-.5 1.5a.5.5 0 0 1-.948-.316l.5-1.5a.5.5 0 0 1 .632-.317zm6 0a.5.5 0 0 1 .316.633l-.5 1.5a.5.5 0 1 1-.948-.316l.5-1.5a.5.5 0 0 1 .632-.317zm.747-8.498a5.001 5.001 0 0 0-9.499-1.004A3.5 3.5 0 1 0 3.5 11H13a3 3 0 0 0 .405-5.973zM8.5 2a4 4 0 0 1 3.976 3.555.5.5 0 0 0 .5.445H13a2 2 0 0 1 0 4H3.5a2.5 2.5 0 1 1 .605-4.926.5.5 0 0 0 .596-.329A4.002 4.002 0 0 1 8.5 2z"/>
</svg>
<p className="day-text">Friday</p>
<p className='day-text'>28.8 c</p>
<p className='day-text'>Partly Coludy</p>
</div>
</div>
<div className="days-count">
<div className="margin">
<svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" fill="currentColor"  class="bi bi-cloud-drizzle icon" viewBox="0 0 16 16">
  <path d="M4.158 12.025a.5.5 0 0 1 .316.633l-.5 1.5a.5.5 0 0 1-.948-.316l.5-1.5a.5.5 0 0 1 .632-.317zm6 0a.5.5 0 0 1 .316.633l-.5 1.5a.5.5 0 0 1-.948-.316l.5-1.5a.5.5 0 0 1 .632-.317zm-3.5 1.5a.5.5 0 0 1 .316.633l-.5 1.5a.5.5 0 0 1-.948-.316l.5-1.5a.5.5 0 0 1 .632-.317zm6 0a.5.5 0 0 1 .316.633l-.5 1.5a.5.5 0 1 1-.948-.316l.5-1.5a.5.5 0 0 1 .632-.317zm.747-8.498a5.001 5.001 0 0 0-9.499-1.004A3.5 3.5 0 1 0 3.5 11H13a3 3 0 0 0 .405-5.973zM8.5 2a4 4 0 0 1 3.976 3.555.5.5 0 0 0 .5.445H13a2 2 0 0 1 0 4H3.5a2.5 2.5 0 1 1 .605-4.926.5.5 0 0 0 .596-.329A4.002 4.002 0 0 1 8.5 2z"/>
</svg>
<p className="day-text">Saturday</p>
<p className='day-text'>28.8 c</p>
<p className='day-text'>Partly Coludy</p>
</div>
</div>
</div>
      </div>
      <h1 className="today-text">Todays Weather in Hyderabad</h1>
     
      <div className="present-day-container">
   
         <div className="present-day">
         <div className="margin">
          <p>Sunrise    06:17 AM</p>
          <p>SunSet 07:11 Pm </p>
          </div> 
          </div>

          <div className="present-day">
            <div className="margin-left">
          <p>Moonrise    03:49 pM</p>
          <p>Moonset 01:56 Am </p>
          </div>
          </div> 
         
          <div className="present-day">
            <div className="margin-left">
              <div classNAme="margin-left-text">
          <p>Max.</p>
          <p>30.7 C </p>
          </div>
          </div> 
          </div>
          <div className="present-day">
            <div className="margin-left">
          <p>Min.</p>
          <p>24 C </p>
          </div>
          </div> 
          <div className="present-day">
            <div className="margin-left">
          <p>Avg</p>
          <p>06:08 pm </p>
          </div>
          </div> 
          <div className="present-day">
            <div className="margin-left">
          <p> Precip</p>
          <p>9.19 mm</p>
          </div>
          </div> 
          <div className="present-day">
            <div className="margin-left">
          <p>Max.Wind</p>
          <p>13.0 kph</p>
          </div>
          </div>
          </div> 
      
      
    </div>
  
  );
};


export default Weather;