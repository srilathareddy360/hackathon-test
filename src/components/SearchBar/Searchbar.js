import React, { useEffect, useState } from 'react'
import TextField from "@mui/material/TextField";
import Weather from "../Weatherforecast/Weather";
import "./searchbar.css";

function SearchBar() {
  const [userInput, setUserInput] = useState("")
  const handleUserInput = (e)=>{
    const input = e.target.value;
    setUserInput(input)
  }
  return (
    <div className="main">
      
      <div className="search">
        <TextField
          id="outlined-basic"
          variant="outlined"
          fullWidth
          label="Please enter city Name, US Zip Code, Canada Postal code,UK PostCode,ip,metar,etc."
          onChange={handleUserInput} 
          value={userInput}
        />
      </div>
      <Weather/>
    </div>
  );
}




export default SearchBar;