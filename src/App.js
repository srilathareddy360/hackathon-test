import './App.css';
import SearchBar from './components/SearchBar/Searchbar';
import Weather from './components/Weatherforecast/Weather';
import { BrowserRouter , Routes, Route } from "react-router-dom";

function App() {
  return (
    
    <>

   <BrowserRouter>
      <SearchBar>
      <Routes>

        
        <Route path="/" element={<Weather/>}></Route>

      </Routes>
      </SearchBar>
    </BrowserRouter>

   </>
  );
}

export default App;
